# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2020-12-17
### Added
- Initial Version
- feat: unit testing
- feat: ability to choose the report type
- feat: parse results object into end verbose.
- feat: ni package building configuration
